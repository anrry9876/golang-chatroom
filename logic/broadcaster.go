package logic

import (
	"expvar"
	"fmt"
	"log"
)

func init() {
	expvar.Publish("message_queue", expvar.Func(calcMessageQueueLen))
}

func calcMessageQueueLen() interface{} {
	fmt.Println("===len=:", len(Broadcaster.messageChannel))
	return len(Broadcaster.messageChannel)
}

// 單例模式的廣播企，只會展生一個廣播器的實例
type broadcaster struct{
	users map[string]*User
	// 使用者進入聊天室，透過這個頻道告訴大家
	enteringChannel chan *User
	// 使用者離開聊天室，透過這個頻道告訴大家
	leavingChannel chan *User
	// 訊息廣播的頻道，把訊息發送給所有有訂閱這個頻道的使用者
	messageChannel chan *Message
	// 判斷該暱稱使用者使否可以進入聊天室(重複與否) : true false
	// 用來接手使用者暱稱，方便 BroadCaster 所在的 goroutine 夠不使用鎖的判斷暱稱是否存在
	checkUserChannel chan string
	// 用來確認該使用者暱稱是否存在
	checkUserCanInChannel chan bool

	// 獲取使用者列表
	requestUsersChannel chan struct{}
	usersChannel        chan []*User
}

var Broadcaster = &broadcaster{
	users: make(map[string]*User),

	enteringChannel: make(chan *User),
	leavingChannel:  make(chan *User),
	messageChannel:  make(chan *Message, 32),

	checkUserChannel:      make(chan string),
	checkUserCanInChannel: make(chan bool),

	requestUsersChannel: make(chan struct{}),
	usersChannel:        make(chan []*User),
}

// Start 啟動廣播器
// 需要在一個 Goroutine 當中執行 app 一樣
func (b *broadcaster) Start() {
	for {
		select {
		case user := <-b.enteringChannel:
			// New User
			b.users[user.NickName] = user

			//OfflineProcessor.Send(user)
		case user := <-b.leavingChannel:
			// User Leave
			delete(b.users, user.NickName)
			//  避免 goroutine 洩漏
			user.CloseMessageChannel()
		case msg := <-b.messageChannel:
			// 給所有在線上的用戶發訊息
			for _, user := range b.users {
				if user.UID == msg.User.UID {
					continue
				}
				user.MessageChannel <- msg
			}
			//OfflineProcessor.Save(msg)
		case nickname := <-b.checkUserChannel:
			if _, ok := b.users[nickname]; ok {
				b.checkUserCanInChannel <- false
			} else {
				b.checkUserCanInChannel <- true
			}
		case <-b.requestUsersChannel:
			userList := make([]*User, 0, len(b.users))
			for _, user := range b.users {
				userList = append(userList, user)
			}

			b.usersChannel <- userList
		}
	}
}

func (b *broadcaster) UserEntering(u *User) {
	b.enteringChannel <- u
}

func (b *broadcaster) UserLeaving(u *User) {
	b.leavingChannel <- u
}

func (b *broadcaster) Broadcast(msg *Message) {
	if len(b.messageChannel) >= 32 {
		log.Println("broadcast queue 以滿")
	}
	b.messageChannel <- msg
}

func (b *broadcaster) CanEnterRoom(nickname string) bool {
	b.checkUserChannel <- nickname

	return <-b.checkUserCanInChannel
}

func (b *broadcaster) GetUserList() []*User {
	b.requestUsersChannel <- struct{}{}
	return <-b.usersChannel
}




