package logic

import (
	"github.com/spf13/cast"
	"time"
)

// 用來判斷訊息在用戶端是如何顯示的
const (
	MsgTypeNormal    = iota // 普通廣播訊息
	MsgTypeWelcome          // 歡迎目前用戶進入聊天是的訊息
	MsgTypeUserEnter        // 使用者進入
	MsgTypeUserLeave        // 使用者退出
	MsgTypeError            // 錯誤訊息
)


type Message struct{
	// 那一個使用者發送的消息
	User    *User     `json:"user"`
	Type    int       `json:"type"`
	Content string    `json:"content"`
	MsgTime time.Time `json:"msg_time"`

	ClientSendTime time.Time `json:"client_send_time"`

	// @ 誰
	Ats []string `json:"ats"`

	// Users []*User `json:"users"`
}

func NewMessage(user *User, content,clientTime string)*Message{
	message := &Message{
		User: user,
		Type: MsgTypeNormal,
		Content: content,
		MsgTime: time.Now(),
	}
	if clientTime != ""{
		message.ClientSendTime = time.Unix(0, cast.ToInt64(clientTime))
	}
	return message
}

func NewWelcomeMessage(user *User) *Message{
	return &Message{
		User:    user,
		Type:    MsgTypeWelcome,
		Content: user.NickName + " 您好，歡迎進入聊天室！",
		MsgTime: time.Now(),
	}
}
func NewUserEnterMessage(user *User) *Message {
	return &Message{
		User:    user,
		Type:    MsgTypeUserEnter,
		Content: user.NickName + " 加入了聊天室",
		MsgTime: time.Now(),
	}
}

func NewUserLeaveMessage(user *User) *Message {
	return &Message{
		User:    user,
		Type:    MsgTypeUserLeave,
		Content: user.NickName + " 離開了聊天室",
		MsgTime: time.Now(),
	}
}

func NewErrorMessage(content string) *Message {
	return &Message{
		User:    System,
		Type:    MsgTypeError,
		Content: content,
		MsgTime: time.Now(),
	}
}
