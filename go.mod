module golangChatServer

go 1.15

require (
	github.com/spf13/cast v1.3.1
	github.com/spf13/viper v1.7.1
	nhooyr.io/websocket v1.8.6
)
