package cmd

import (
	"fmt"
	"golangChatServer/Server"
	"log"
	"net/http"
)

func Run(){
	fmt.Println("Server 啟動.....")
	Server.RegisterHandle()
	log.Fatal(http.ListenAndServe(":2021", nil))
}