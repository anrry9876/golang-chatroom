package Server

import (
	"golangChatServer/logic"
	"log"
	"net/http"
	"nhooyr.io/websocket"
	"nhooyr.io/websocket/wsjson"
)

func  WebSocketHandleFunc(w http.ResponseWriter, req *http.Request){
	// Accept 從客戶端接受 Websocket 交握，http 連線變成 WebSocket 連線。
	// 如果 Origin 網域與主機不同，Accept 就會拒絕交握，除非設定 InsecureSkipVerify 選項（通過第三個參數 AcceptOptions 設定）。
	// 換句話說，預設狀態下部允許 Cross Domain 的連線，如果發生錯誤，Accept 將始終寫入適當的回應
	conn, err := websocket.Accept(w, req, &websocket.AcceptOptions{InsecureSkipVerify: true})
	if err != nil {
		log.Println("websocket accept error:", err)
		return
	}

	// 1. 新用戶進入，建立用戶實例
	token := req.FormValue("token")
	nickname := req.FormValue("nickname")
	if l := len(nickname); l < 2 || l > 20 {
		log.Println("nickname illegal: ", nickname)
		wsjson.Write(req.Context(), conn, logic.NewErrorMessage("非法暱稱，長度 2-20"))
		conn.Close(websocket.StatusUnsupportedData, "nickname illegal!")
		return
	}
	if !logic.Broadcaster.CanEnterRoom(nickname) {
		log.Println("該暱稱以存在：", nickname)
		wsjson.Write(req.Context(), conn, logic.NewErrorMessage("該暱稱以存在！"))
		conn.Close(websocket.StatusUnsupportedData, "nickname exists!")
		return
	}

	userHasToken := logic.NewUser(conn, token, nickname, req.RemoteAddr)

	// 2. 開啟給使用者發送消息的 goroutine
	go userHasToken.SendMessage(req.Context())

	// 3. 給目前近來的用戶發送歡迎訊息
	userHasToken.MessageChannel <- logic.NewWelcomeMessage(userHasToken)

	// 避免 token 洩漏
	tmpUser := *userHasToken
	user := &tmpUser
	user.Token = ""

	// 讓所有用戶知道新用戶到來
	msg := logic.NewUserEnterMessage(user)
	logic.Broadcaster.Broadcast(msg)

	// 4. 將用戶加入廣播器列表當中
	logic.Broadcaster.UserEntering(user)
	log.Println("user:", nickname, "joins chat")

	// 5. 接收使用者訊息
	err = user.ReceiveMessage(req.Context())

	// 6. 使用者離開
	logic.Broadcaster.UserLeaving(user)
	msg = logic.NewUserLeaveMessage(user)
	logic.Broadcaster.Broadcast(msg)
	log.Println("user:", nickname, "leaves chat")

	// 根據讀取時的錯誤執行不同的 Close
	if err == nil {
		conn.Close(websocket.StatusNormalClosure, "")
	} else {
		log.Println("read from client error:", err)
		conn.Close(websocket.StatusInternalError, "Read from client error")
	}
}
