package Server

import (
	"golangChatServer/logic"
	"net/http"
)

func RegisterHandle(){
	go logic.Broadcaster.Start()
	http.HandleFunc("/ws", WebSocketHandleFunc)
}